# Changelog

Minds Mobile

## 4.12.0 - 2021-05-05

### Changed

- Images pre-load on feeds
- New chat integration
- Improved deeplink support
- New sessions screen
- Bug fixing

## 4.11.1 - 2021-04-23

### Changed

- Bug fixing

## 4.11.0 - 2021-03-25

### Changed

- Channel screen styling improvements
- Updated react-native to 0.64
- Bug fixing

## 4.10.0 - 2021-03-20

### Changed

- Two-Factor Auth
- Bug fixing
